module.exports = {
    "env": {
        "browser": true
    },
    "extends": "eslint:recommended",
    "rules": {
        "block-scoped-var": 2,
        "brace-style": [
            2,
            "stroustrup",
            {
                "allowSingleLine": true
            }
        ],
        "camelcase": [
            2,
            {
                "properties": "never"
            }
        ],
        "comma-dangle": [
            2,
            "never"
        ],
        "comma-spacing": [
            2,
            {
                "before": false,
                "after": true
            }
        ],
        "comma-style": 2,
        "consistent-return": 2,
        "curly": 2,
        "default-case": 2,
        "dot-notation": [
            2,
            {
                "allowKeywords": false
            }
        ],
        "eol-last": 2,
        "eqeqeq": 2,
        "indent": [
            2,
            2
        ],
        "key-spacing": [
            2,
            {
                "beforeColon": false,
                "afterColon": true
            }
        ],
        "keyword-spacing": 2,
        "linebreak-style": [
            2,
            "unix"
        ],
        "new-cap": [
            2,
            {
                "newIsCap": true
            }
        ],
        "no-alert": 2,
        "no-caller": 2,
        "no-cond-assign": [
            2,
            "always"
        ],
        "no-console": 1,
        "no-constant-condition": 2,
        "no-debugger": 2,
        "no-dupe-keys": 2,
        "no-duplicate-case": 2,
        "no-empty": 2,
        "no-else-return": 2,
        "no-eq-null": 2,
        "no-eval": 1,
        "no-ex-assign": 2,
        "no-extend-native": 2,
        "no-extra-bind": 2,
        "no-extra-boolean-cast": 2,
        "no-extra-parens": 2,
        "no-extra-semi": 2,
        "no-fallthrough": 2,
        "no-floating-decimal": 2,
        "no-func-assign": 2,
        "no-implied-eval": 2,
        "no-inner-declarations": 2,
        "no-invalid-regexp": 2,
        "no-irregular-whitespace": 2,
        "no-lone-blocks": 2,
        "no-loop-func": 2,
        "no-multi-str": 2,
        "no-multiple-empty-lines": 2,
        "no-native-reassign": 2,
        "no-new-func": 2,
        "no-new-wrappers": 2,
        "no-new-object": 2,
        "no-obj-calls": 2,
        "no-octal": 2,
        "no-octal-escape": 2,
        "no-param-reassign": 2,
        "no-proto": 2,
        "no-redeclare": 2,
        "no-return-assign": 2,
        "no-script-url": 2,
        "no-self-compare": 2,
        "no-sequences": 2,
        "no-spaced-func": 2,
        "no-sparse-arrays": 2,
        "no-throw-literal": 2,
        "no-trailing-spaces": 2,
        "no-underscore-dangle": 2,
        "no-unreachable": 2,
        "no-with": 2,
        "one-var": [
            2,
            "never"
        ],
        "padded-blocks": [
            2,
            "never"
        ],
        "quotes": [
            2,
            "single",
            {
                "avoidEscape": true
            }
        ],
        "radix": 2,
        "semi": [
            2,
            "always"
        ],
        "semi-spacing": 2,
        "space-before-blocks": 2,
        "space-before-function-paren": [
            2,
            "never"
        ],
        "space-infix-ops": 2,
        "spaced-comment": [
            2,
            "always",
            {
                "exceptions": [
                    "-",
                    "*"
                ],
                "markers": [
                    "=",
                    "!"
                ]
            }
        ],
        "use-isnan": 2,
        "vars-on-top": 2,
        "wrap-iife": [
            2,
            "inside"
        ],
        "yoda": 2
    }
};
