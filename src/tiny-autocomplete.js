/**
 * Tiny Autocomplete
 * Small and fast autocomplete plugin.
 * Written by Johan Halse, https://twitter.com/hejsna, johan@varvet.se
 * License: http://johanhalse.mit-license.org
 * @version 3.0
 */
(function() {
  'use strict';

  var TinyAutocomplete = function(el, options) {
    var i;
    this.settings = {
      url: 'http://example.com',
      minChars: 2,
      queryProperty: 'q',
      method: 'GET',
      data: undefined,
      markAsBold: false,
      jsonProcessor: undefined,
      listTemplate: '<ul class="autocomplete-list" />',
      itemTemplate: '<li class="autocomplete-item">{{title}}</li>',
      alwaysAllowEnter: false,
      keyboardDelay: 300,
      scrollOnFocus: this.useScrollOnFocus(),
      onSelectItem: undefined
    };

    for (i in options) {
      this.settings[i] = options[i];
    }

    this.field = el;
    this.container = this.createContainer(el);
    this.list = this.create(this.settings.listTemplate);
    this.selectedIndex = -1;
    this.numItems = 0;
    this.items = [];
    this.listeners = {};
    this.lastQuery = undefined;
    this.field.addEventListener('keyup', this.debounce(this.onKeyUp.bind(this), this.keyboardDelay), false);
    this.field.addEventListener('keydown', this.onKeyDown.bind(this), false);
    this.field.addEventListener('focus', this.onFocus.bind(this), false);
    this.list.addEventListener('mousedown', this.onClick.bind(this), false);

    this.on('key/arrow-down', this.onArrowDown.bind(this));
    this.on('key/arrow-up', this.onArrowUp.bind(this));
  };

  TinyAutocomplete.prototype = {
    // Thank you ppk, http://www.quirksmode.org/js/findpos.html
    position: function() {
      var obj = this.field;
      var curleft = 0;
      var curtop = 0;
      if (obj.offsetParent) {
        do { // eslint-disable-line no-cond-assign
          curleft += obj.offsetLeft;
          curtop += obj.offsetTop;
        } while (obj = obj.offsetParent);
        return { left: curleft, top: curtop };
      }

      return undefined;
    },

    create: function(markup) {
      var div = document.createElement('div');
      div.innerHTML = markup;
      return div.firstChild;
    },

    createContainer: function(el) {
      var parent;
      el.classList.add('autocomplete-field');
      el.setAttribute('autocomplete', 'off');
      el.insertAdjacentHTML('afterend', '<div class="autocomplete" />');
      parent = el.nextSibling;
      parent.appendChild(el);
      return parent;
    },

    viewportHeight: function() {
      return Math.max(window.innerHeight || 0);
    },

    isTouchDevice: function() {
      return !!('ontouchstart' in window);
    },

    useScrollOnFocus: function() {
      return this.isTouchDevice() || this.viewportHeight() < 500;
    },

    // Lifted from http://davidwalsh.name/javascript-debounce-function
    debounce: function(func, wait, immediate) {
      var timeout;
      return function() {
        var context = this;
        var args = arguments;
        var later = function() {
          timeout = null;
          if (!immediate) {
            func.apply(context, args);
          }
        };
        var callNow = immediate && !timeout;
        clearTimeout(timeout);
        timeout = setTimeout(later, wait);
        if (callNow) {
          func.apply(context, args);
        }
      };
    },

    matchArray: function(val, arr) {
      var i;
      var j;
      var r = [];
      for (i = 0; i < arr.length; i++) {
        for (j in arr[i]) {
          if (arr[i][j].toLowerCase && arr[i][j].toLowerCase().indexOf( val.toLowerCase() ) > -1 ||
              arr[i][j] === val) {
            r.push(arr[i]);
            break;
          }
        }
      }

      return r;
    },

    strongText: function(data, query) {
      var markHitText = this.markHitText.bind(this);
      if (this.settings.markAsBold === false) {
        return data;
      }
      return data.map(function(item) {
        return markHitText(item, query);
      });
    },

    processJSON: function(data, jsonProcessor, query) {
      var processedData = typeof data === 'string' ? JSON.parse(data) : data;
      if (jsonProcessor !== undefined) {
        return jsonProcessor(processedData, query);
      }
      return processedData;
    },

    render: function(template, vars) {
      return template.replace(/{{\s*[\w]+\s*}}/g, function(v) {
        return vars[v.substr(2, v.length - 4)];
      });
    },

    renderItem: function(item) {
      return this.render(this.settings.itemTemplate, item);
    },

    markHitText: function(jsonData, str) {
      var i; var j;
      var rv = {};
      var replacements;
      var word;
      var words;

      if (this.settings.markAsBold === false) {
        return jsonData;
      }

      words = str.split(' ');

      for (i in jsonData) {
        if (typeof jsonData[i] === 'string' && i !== 'template') {
          replacements = [str];
          for (j = 0; j < words.length; j++) {
            // Remove non-alphanumerics
            word = words[j].trim().replace(/[^a-ö0-9]/gi, '');
            if (word.length > 0) {
              replacements.push(word);
            }
          }
          rv[i] = jsonData[i].replace( new RegExp('(' + replacements.join('|') + ')', 'gi'), '<strong>$1</strong>' );
        }
      }
      return rv;
    },

    // This block deals with emitting events
    on: function(ev, cb) {
      this.listeners[ev] = this.listeners[ev] || [];
      this.listeners[ev].push(cb);
      return this;
    },

    off: function(ev, cb) {
      if (cb === undefined) {
        delete this.listeners[ev];
        return this;
      }

      this.listeners[ev] = this.listeners[ev].filter(function(item) {
        if (item === cb) {
          return false;
        }

        return true;
      });
      return this;
    },

    emit: function(ev) {
      var args = Array.prototype.slice.call(arguments);
      var that = this;
      args.shift();
      if (this.listeners[ev] !== undefined) {
        this.listeners[ev].forEach(function(item) {
          item.apply(that, args);
        });
      }

      return this;
    },
    // End event emitting block

    getify: function(data) {
      var queries = [];
      var key;
      for (key in data) {
        queries.push(key + '=' + data[key]);
      }

      return '?' + queries.join('&');
    },

    request: function(url, data, cb) {
      var getUrl = undefined;
      if (this.settings.method.toLowerCase() === 'get') {
        getUrl = url + this.getify(data);
      }
      return this.$.ajax({
        url: getUrl || url,
        type: this.settings.method,
        data: data,
        success: cb
      });
    },

    onSuccess: function(query) {
      return function(code, data) {
        this.items = this.processJSON(data, this.settings.jsonProcessor, query);

        // Strong text is applied only to rendered output, don't change
        // the actual JSON results!
        this.list.innerHTML = this.strongText(this.items, query).map(
          this.renderItem.bind(this)
        ).join('');

        this.selectedIndex = -1;
        this.numItems = this.items.length;
        this.showList();
        return this.items;
      };
    },

    onKeyDown: function(e) {
      if (e.keyCode === 13) { // Enter
        this.emit('key/enter', e);
        if (this.settings.alwaysAllowEnter === false && this.selectedIndex < 0) {
          e.preventDefault();
        }
        if (this.settings.onSelectItem !== undefined) {
          this.settings.onSelectItem.apply(this, [e.target, this.items[this.selectedIndex]]);
        }
        this.emit('submit', this);
      }
      if (e.keyCode === 27) { // Esc
        this.emit('key/escape', e);
        this.hideList();
      }
      if (e.keyCode === 38) { // Up
        e.preventDefault();
        this.emit('key/arrow-up', e);
      }
      if (e.keyCode === 40) { // Down
        e.preventDefault();
        this.emit('key/arrow-down', e);
      }
    },

    onKeyUp: function() {
      var requestData = {};
      var params = {};

      if (this.lastQuery === this.field.value) {
        return false;
      }

      if (this.field.value.length < this.settings.minChars) {
        this.hideList();
        return false;
      }

      this.lastQuery = this.field.value;

      if (this.settings.data === undefined) {
        requestData[this.settings.queryProperty] = this.field.value;
        this.request(this.settings.url, requestData, this.onSuccess(this.field.value).bind(this));
      }
      else {
        params[this.settings.queryProperty] = this.field.value;
        this.onSuccess(this.field.value).bind(this)(200, this.matchArray(this.field.value, this.settings.data));
      }
      return true;
    },

    onArrowDown: function(e) {
      e.preventDefault();
      this.selectedIndex++;
      if (this.selectedIndex >= this.numItems) {
        this.selectedIndex = this.numItems - 1;
      }
      this.markSelectedItem(this.selectedIndex);
    },

    onArrowUp: function(e) {
      e.preventDefault();
      this.selectedIndex--;
      if (this.selectedIndex < 0) {
        this.selectedIndex = -1;
      }
      this.markSelectedItem(this.selectedIndex);
    },

    markSelectedItem: function(index) {
      var oldSelectedItem = this.container.querySelector('.autocomplete-item--selected');
      var items = this.container.querySelectorAll('.autocomplete-item');
      if (oldSelectedItem !== null) {
        oldSelectedItem.classList.remove('autocomplete-item--selected');
      }

      if (items.length > index && index > -1) {
        items[index].classList.add('autocomplete-item--selected');
      }
    },

    getIndexOfItem: function(item) {
      var i = 0;
      var items = this.container.querySelectorAll('.autocomplete-item');
      for (i = 0; i < items.length; i++) {
        if (item === items[i]) {
          return i;
        }
      }

      return -1;
    },

    onClick: function(e) {
      var clickedIndex = this.getIndexOfItem(e.target);
      e.preventDefault();
      if (this.settings.onSelectItem !== undefined) {
        this.settings.onSelectItem.apply(this, [e.target, this.items[clickedIndex]]);
      }
      return e.target;
    },

    onFocus: function() {
      var offset = this.position().top;
      if (this.settings.scrollOnFocus) {
        setTimeout(function() {
          window.scrollTo(0, offset);
        }, 0);
      }
      return offset;
    },

    showList: function() {
      this.container.appendChild(this.list);
    },

    hideList: function() {
      this.selectedIndex = -1;
      this.lastQuery = undefined;
      if (this.container.contains(this.list) === true) {
        this.container.removeChild(this.list);
      }
    }
  };

  window.TinyAutocomplete = TinyAutocomplete;
})();
