var conf = require('./karma-conf.js');

module.exports = function(config) {
  conf(config);
  config.set({
    singleRun: true,
    autoWatch: false,
    preprocessors: {
      'src/*.js': 'coverage'
    },
    reporters: ['progress', 'coverage']
  });
};

