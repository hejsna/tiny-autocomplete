// Jasmine globals
// --------------------------
/* global jasmine */
/* global describe */
/* global it */
/* global expect */
/* global beforeEach */
/* global afterEach */
/* global spyOn */

// Custom globals
/* global TinyAutocomplete */
/* global TestResponses */
/* global keyEvents */
// --------------------------

describe('Autocomplete plugin with default options', function() {
  var tinyAutocomplete;
  var parent;
  var input;
  var requestSpy;

  beforeEach(function() {
    parent = document.createElement('div');
    input = document.createElement('input');
    parent.appendChild(input);

    requestSpy = spyOn(TinyAutocomplete.prototype, 'request');
    spyOn(TinyAutocomplete.prototype, 'onKeyUp').and.callThrough();
    spyOn(TinyAutocomplete.prototype, 'onKeyDown').and.callThrough();
    spyOn(TinyAutocomplete.prototype, 'onSuccess').and.callThrough();
    spyOn(TinyAutocomplete.prototype, 'onFocus').and.callThrough();
    spyOn(keyEvents.enterEvent, 'preventDefault');

    tinyAutocomplete = new TinyAutocomplete(input);
    document.body.appendChild(tinyAutocomplete.container);
  });

  afterEach(function() {
    document.body.removeChild(tinyAutocomplete.container);
  });

  it('can create markup', function() {
    var a = document.createElement('a');
    var p = document.createElement('p');
    a.innerHTML = 'text';
    p.appendChild(a);

    expect(tinyAutocomplete.create('<div />')).toEqual(document.createElement('div'));
    expect(tinyAutocomplete.create('<p><a>text</a></p>')).toEqual(p);
  });

  it('sets up proper markup', function() {
    // Markup is supposed to look like this:
    // <div class="autocomplete">
    //   <input class="autocomplete-field" autocomplete="off" />
    // </div>
    var div = document.createElement('div');
    div.classList.add('autocomplete');
    div.innerHTML = '<input class="autocomplete-field" autocomplete="off" />';

    expect(tinyAutocomplete.createContainer(input)).toEqual(div);
  });

  it('can debounce a function', function(done) {
    var now = +new Date();

    var debouncedFunction = tinyAutocomplete.debounce(function() {
      expect(+new Date()).toBeGreaterThan(now + 14);
      done();
    }, 15);
    debouncedFunction();
  });

  it('can run a debounced function immediately', function(done) {
    var now = +new Date();

    var debouncedFunction = tinyAutocomplete.debounce(function() {
      expect(+new Date()).toBeLessThan(now + 3);
      done();
    }, 15, true);
    debouncedFunction();
  });

  it('processes JSON when given a callback', function() {
    var testData = '["foo", "bar", "baz"]';
    var parsedTestData = ['foo', 'bar', 'baz'];
    var processedTestData = ['foo0', 'bar1', 'baz2'];
    var processor = function(data) {
      return data.map(function(item, i) {
        return item + i;
      });
    };

    expect(tinyAutocomplete.processJSON(testData)).toEqual(parsedTestData);
    expect(tinyAutocomplete.processJSON(testData, processor)).toEqual(processedTestData);
  });

  it('can interpolate a template string', function() {
    var data = { text: 'foo', url: 'http://example.com', link: '<strong>bar</strong>' };
    var template = '<div class="tmpl">{{text}}<a href="{{url}}">{{link}}</a></div>';
    var expectedTemplate = '<div class="tmpl">foo<a href="http://example.com"><strong>bar</strong></a></div>';

    expect(tinyAutocomplete.render(template, data)).toEqual(expectedTemplate);
  });

  it('can bind and unbind event callbacks', function() {
    var argComparison0 = function bars(arg1, arg2) {
      expect(arg1).toEqual('bar');
      expect(arg2).toEqual('baz');
    };
    var argComparison1 = function apples(arg1, arg2) {
      expect(arg1).toEqual('apple');
      expect(arg2).toEqual('orange');
    };

    // Attach two callbacks, unbind one, and test emit
    tinyAutocomplete.on('foo', argComparison0);
    tinyAutocomplete.on('foo', argComparison1);
    tinyAutocomplete.off('foo', argComparison0);
    tinyAutocomplete.emit('foo', 'apple', 'orange');

    // Re-attach first emit, unbind everything, try another emit
    tinyAutocomplete.on('foo', argComparison0);
    tinyAutocomplete.off('foo');
    tinyAutocomplete.emit('foo', 'fruit', 'bats');
  });

  it('reacts to keyup events', function(done) {
    tinyAutocomplete.settings.keyboardDelay = 20;
    tinyAutocomplete.field.dispatchEvent(keyEvents.keyUpEvent);

    setTimeout(function() {
      expect(tinyAutocomplete.onKeyUp).toHaveBeenCalled();
      done();
    }, tinyAutocomplete.settings.keyboardDelay + 5);
  });

  it('reacts to keydown events', function() {
    tinyAutocomplete.field.dispatchEvent(keyEvents.arrowDownEvent);
    expect(tinyAutocomplete.onKeyDown).toHaveBeenCalled();
  });

  it('doesn\'t search if value is unchanged', function(done) {
    tinyAutocomplete.field.value = 'aa';
    tinyAutocomplete.settings.keyboardDelay = 20;
    tinyAutocomplete.field.dispatchEvent(keyEvents.keyUpEvent);

    setTimeout(function() {
      tinyAutocomplete.field.dispatchEvent(keyEvents.keyUpEvent);
      expect(tinyAutocomplete.request).toHaveBeenCalled();
    }, tinyAutocomplete.settings.keyboardDelay + 5);

    setTimeout(function() {
      expect(tinyAutocomplete.request.calls.count()).toEqual(1);
      done();
    }, tinyAutocomplete.settings.keyboardDelay * 2 + 5);
  });

  it('emits its own events from keydown', function() {
    var keyFunctions = {
      arrowUp: function() {},
      arrowDown: function() {}
    };

    spyOn(keyFunctions, 'arrowUp');
    spyOn(keyFunctions, 'arrowDown');
    tinyAutocomplete.on('key/arrow-up', keyFunctions.arrowUp);
    tinyAutocomplete.on('key/arrow-down', keyFunctions.arrowDown);

    tinyAutocomplete.field.dispatchEvent(keyEvents.arrowDownEvent);
    tinyAutocomplete.field.dispatchEvent(keyEvents.arrowUpEvent);

    expect(keyFunctions.arrowDown).toHaveBeenCalled();
    expect(keyFunctions.arrowUp).toHaveBeenCalled();
  });

  it('sets the correct number of items', function() {
    var testData = '[{"title": "foo"}, {"title": "bar"}]';
    tinyAutocomplete.onSuccess({params: {q: 'aa'}}).bind(tinyAutocomplete)(200, testData);
    expect(tinyAutocomplete.numItems).toEqual(2);
  });

  it('sets correct selectedIndex after keydown', function() {
    var testData = '[{"title": "foo"}, {"title": "bar"}]';
    tinyAutocomplete.onSuccess({params: {q: 'aa'}}).bind(tinyAutocomplete)(200, testData);

    tinyAutocomplete.field.dispatchEvent(keyEvents.arrowDownEvent);
    expect(tinyAutocomplete.selectedIndex).toEqual(0);

    tinyAutocomplete.field.dispatchEvent(keyEvents.arrowDownEvent);
    expect(tinyAutocomplete.selectedIndex).toEqual(1);

    tinyAutocomplete.field.dispatchEvent(keyEvents.arrowDownEvent);
    expect(tinyAutocomplete.selectedIndex).toEqual(1);
  });

  it('sets correct selectedIndex after keyup', function() {
    var testData = '[{"title": "foo"}, {"title": "bar"}]';
    tinyAutocomplete.onSuccess({params: {q: 'aa'}}).bind(tinyAutocomplete)(200, testData);

    tinyAutocomplete.field.dispatchEvent(keyEvents.arrowDownEvent);
    expect(tinyAutocomplete.selectedIndex).toEqual(0);

    tinyAutocomplete.field.dispatchEvent(keyEvents.arrowUpEvent);
    expect(tinyAutocomplete.selectedIndex).toEqual(-1);

    tinyAutocomplete.field.dispatchEvent(keyEvents.arrowUpEvent);
    expect(tinyAutocomplete.selectedIndex).toEqual(-1);
  });

  it('resets selectedIndex when new data is received', function() {
    var testData = '[{"title": "foo"}, {"title": "bar"}]';
    tinyAutocomplete.onSuccess({params: {q: 'aa'}}).bind(tinyAutocomplete)(200, testData);

    tinyAutocomplete.field.dispatchEvent(keyEvents.arrowDownEvent);
    expect(tinyAutocomplete.selectedIndex).toEqual(0);

    tinyAutocomplete.field.dispatchEvent(keyEvents.arrowDownEvent);
    expect(tinyAutocomplete.selectedIndex).toEqual(1);

    tinyAutocomplete.onSuccess({params: {q: 'aa'}}).bind(tinyAutocomplete)(200, testData);
    expect(tinyAutocomplete.selectedIndex).toEqual(-1);
  });

  it('resets selectedIndex when search string is too short', function() {
    var testData = '[{"title": "foo"}, {"title": "bar"}]';
    tinyAutocomplete.field.value = 'aa';
    tinyAutocomplete.onSuccess({params: {q: 'aa'}}).bind(tinyAutocomplete)(200, testData);

    tinyAutocomplete.field.dispatchEvent(keyEvents.arrowDownEvent);
    expect(tinyAutocomplete.selectedIndex).toEqual(0);

    tinyAutocomplete.field.value = 'a';
    tinyAutocomplete.onKeyUp();
    expect(tinyAutocomplete.selectedIndex).toEqual(-1);
  });

  it('resets lastQuery when search string is too short', function() {
    tinyAutocomplete.field.value = 'aa';
    tinyAutocomplete.onKeyUp();

    tinyAutocomplete.field.value = 'a';
    tinyAutocomplete.onKeyUp();
    expect(tinyAutocomplete.lastQuery).toEqual(undefined);
  });

  it('doesn\'t perform a request if string length is too short', function() {
    tinyAutocomplete.field.value = 'a';
    expect(tinyAutocomplete.onKeyUp()).toEqual(false);
  });

  // it('performs a request if string length is long enough', function() {
  //   jasmine.Ajax.install();
  //   tinyAutocomplete.field.value = 'aa';
  //   tinyAutocomplete.onKeyUp();
  //   jasmine.Ajax.requests.mostRecent().respondWith(TestResponses.query.success);

  //   expect(tinyAutocomplete.request).toHaveBeenCalled();

  //   jasmine.Ajax.uninstall();
  // });

  // it('clears the result list after each new request', function() {
  //   jasmine.Ajax.install();
  //   tinyAutocomplete.field.value = 'aa';
  //   tinyAutocomplete.onKeyUp();
  //   jasmine.Ajax.requests.mostRecent().respondWith(TestResponses.query.success);

  //   tinyAutocomplete.field.value = 'aaa';
  //   tinyAutocomplete.onKeyUp();
  //   jasmine.Ajax.requests.mostRecent().respondWith(TestResponses.query.success);

  //   expect(tinyAutocomplete.container.innerHTML).toEqual('<input class="autocomplete-field" autocomplete="off"><ul class="autocomplete-list"><li class="autocomplete-item">Aardvark</li><li class="autocomplete-item">Fruitbat</li><li class="autocomplete-item">Sloth</li></ul>');

  //   jasmine.Ajax.uninstall();
  // });

  it('can show the result list', function() {
    tinyAutocomplete.showList();
    expect(tinyAutocomplete.container.innerHTML).toEqual('<input class="autocomplete-field" autocomplete="off"><ul class="autocomplete-list"></ul>');
  });

  it('has an idempotent showList function', function() {
    tinyAutocomplete.showList();
    tinyAutocomplete.showList();
    expect(tinyAutocomplete.container.innerHTML).toEqual('<input class="autocomplete-field" autocomplete="off"><ul class="autocomplete-list"></ul>');
  });

  it('can hide the result list', function() {
    tinyAutocomplete.showList();
    expect(tinyAutocomplete.container.innerHTML).toEqual('<input class="autocomplete-field" autocomplete="off"><ul class="autocomplete-list"></ul>');
    tinyAutocomplete.hideList();
    expect(tinyAutocomplete.container.innerHTML).toEqual('<input class="autocomplete-field" autocomplete="off">');
  });

  it('can hide the result list on escape', function() {
    tinyAutocomplete.showList();
    expect(tinyAutocomplete.container.innerHTML).toEqual('<input class="autocomplete-field" autocomplete="off"><ul class="autocomplete-list"></ul>');
    tinyAutocomplete.field.dispatchEvent(keyEvents.escapeEvent);
    expect(tinyAutocomplete.container.innerHTML).toEqual('<input class="autocomplete-field" autocomplete="off">');
  });

  it('hides the result list if field is empty', function() {
    tinyAutocomplete.showList();
    expect(tinyAutocomplete.container.innerHTML).toEqual('<input class="autocomplete-field" autocomplete="off"><ul class="autocomplete-list"></ul>');
    tinyAutocomplete.field.value = '';
    tinyAutocomplete.onKeyUp();
    expect(tinyAutocomplete.container.innerHTML).toEqual('<input class="autocomplete-field" autocomplete="off">');
  });

  it('prevents submit on enter if nothing is selected', function() {
    tinyAutocomplete.field.dispatchEvent(keyEvents.enterEvent);
    expect(keyEvents.enterEvent.preventDefault).toHaveBeenCalled();
  });

  it('has an idempotent hideList function', function() {
    tinyAutocomplete.hideList();
    tinyAutocomplete.hideList();
    expect(tinyAutocomplete.container.innerHTML).toEqual('<input class="autocomplete-field" autocomplete="off">');
  });

  it('uses a default query parameter', function() {
    tinyAutocomplete.field.value = 'aa';
    tinyAutocomplete.onKeyUp();
    expect(requestSpy.calls.mostRecent().args[1]).toEqual({q: 'aa'});
  });

  it('shows a list of items on successful ajax request', function() {
    var testData = '[{"title": "foo"}, {"title": "bar"}]';
    var expectedString = '<input class="autocomplete-field" autocomplete="off"><ul class="autocomplete-list"><li class="autocomplete-item">foo</li><li class="autocomplete-item">bar</li></ul>';
    tinyAutocomplete.onSuccess({params: {q: 'aa'}}).bind(tinyAutocomplete)(200, testData);
    expect(tinyAutocomplete.container.innerHTML).toEqual(expectedString);
  });

  it('finds the correct clicked item', function() {
    var testData = '[{"title": "foo"}, {"title": "bar"}]';
    var selectedItem;
    tinyAutocomplete.onSuccess(200, testData, {params: {q: 'aa'}});
    selectedItem = tinyAutocomplete.container.querySelectorAll('.autocomplete-item')[1];

    // TODO: We have a triggerClick helper set up which will help us
    // send a proper, in-browser click event. Having the damnedest time
    // making that work with the spies, though. So for now we just
    // trigger the onClick function immediately and expect it to return
    // the e.target value.
    expect(tinyAutocomplete.onClick({
      preventDefault: function() {},
      target: selectedItem
    })).toEqual(selectedItem);
  });

  it('defaults to scrollOnFocus if viewport height is less than 500', function() {
    expect(tinyAutocomplete.useScrollOnFocus()).toEqual(true);
    tinyAutocomplete.isTouchDevice = function() { return false; };
    expect(tinyAutocomplete.useScrollOnFocus()).toEqual(true);
    tinyAutocomplete.viewportHeight = function() { return 1000; };
    expect(tinyAutocomplete.useScrollOnFocus()).toEqual(false);
  });

  it('defaults to scrollOnFocus if using touch device', function() {
    tinyAutocomplete.isTouchDevice = function() { return true; };
    expect(tinyAutocomplete.useScrollOnFocus()).toEqual(true);
    tinyAutocomplete.viewportHeight = function() { return 1000; };
    expect(tinyAutocomplete.useScrollOnFocus()).toEqual(true);
  });

  it('finds correct viewport height', function() {
    expect(tinyAutocomplete.viewportHeight()).toEqual(400);
  });

  it('finds its own Y position', function() {
    expect(tinyAutocomplete.position()).toEqual({
      top: 10,
      left: 10
    });
    tinyAutocomplete.field.style.cssText = 'margin-top: 200px';
    expect(tinyAutocomplete.position()).toEqual({
      top: 208,
      left: 10
    });
  });

  it('scrolls to correct position when focused', function() {
    tinyAutocomplete.field.focus();
    expect(tinyAutocomplete.onFocus).toHaveBeenCalled();
  });
});
