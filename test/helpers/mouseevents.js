var triggerClick = function(el) {
  var ev = document.createEvent('MouseEvent');
  ev.initMouseEvent(
    'click',
    true,
    true,
    window,
    null,
    0, 0, 0, 0,
    false, false, false, false,
    0, null
  );
  el.dispatchEvent(ev);
};

var triggerBrowserClick = function(el) {
  var rect = el.getBoundingClientRect();
  window.top.callPhantom({
    type: 'mousedown',
    x: rect.left + rect.width / 2,
    y: rect.top + rect.height / 2
  });
};

var screenshot = function() {
  window.top.callPhantom({ type: 'render' });
};

var resize = function(width, height) {
  window.top.callPhantom({
    type: 'resize',
    width: width,
    height: height
  });
};
