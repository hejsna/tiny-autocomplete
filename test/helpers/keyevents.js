var keyEvents = {};
keyEvents.keyUpEvent = document.createEvent('Events');
keyEvents.arrowDownEvent = document.createEvent('Events');
keyEvents.arrowUpEvent = document.createEvent('Events');
keyEvents.enterEvent = document.createEvent('Events');
keyEvents.escapeEvent = document.createEvent('Events');

keyEvents.keyUpEvent.initEvent('keyup', true, true);
keyEvents.keyUpEvent.view = window;
keyEvents.keyUpEvent.altKey = false;
keyEvents.keyUpEvent.ctrlKey = false;
keyEvents.keyUpEvent.shiftKey = false;
keyEvents.keyUpEvent.metaKey = false;
keyEvents.keyUpEvent.keyCode = 0;
keyEvents.keyUpEvent.charCode = 'a';

keyEvents.arrowDownEvent.initEvent('keydown', true, true);
keyEvents.arrowDownEvent.view = window;
keyEvents.arrowDownEvent.altKey = false;
keyEvents.arrowDownEvent.ctrlKey = false;
keyEvents.arrowDownEvent.shiftKey = false;
keyEvents.arrowDownEvent.metaKey = false;
keyEvents.arrowDownEvent.keyCode = 40;
keyEvents.arrowDownEvent.charCode = 'a';

keyEvents.arrowUpEvent.initEvent('keydown', true, true);
keyEvents.arrowUpEvent.view = window;
keyEvents.arrowUpEvent.altKey = false;
keyEvents.arrowUpEvent.ctrlKey = false;
keyEvents.arrowUpEvent.shiftKey = false;
keyEvents.arrowUpEvent.metaKey = false;
keyEvents.arrowUpEvent.keyCode = 38;
keyEvents.arrowUpEvent.charCode = 'a';

keyEvents.enterEvent.initEvent('keydown', true, true);
keyEvents.enterEvent.view = window;
keyEvents.enterEvent.altKey = false;
keyEvents.enterEvent.ctrlKey = false;
keyEvents.enterEvent.shiftKey = false;
keyEvents.enterEvent.metaKey = false;
keyEvents.enterEvent.keyCode = 13;
keyEvents.enterEvent.charCode = 'a';

keyEvents.escapeEvent.initEvent('keydown', true, true);
keyEvents.escapeEvent.view = window;
keyEvents.escapeEvent.altKey = false;
keyEvents.escapeEvent.ctrlKey = false;
keyEvents.escapeEvent.shiftKey = false;
keyEvents.escapeEvent.metaKey = false;
keyEvents.escapeEvent.keyCode = 27;
keyEvents.escapeEvent.charCode = 'a';
