// Jasmine globals
// --------------------------
/* global describe */
/* global it */
/* global expect */
/* global beforeEach */
/* global spyOn */

// Custom globals
/* global TinyAutocomplete */
/* global keyEvents */
// --------------------------

describe('Autocomplete plugin with overridden options', function() {
  var parent;
  var input;
  var requestSpy;

  beforeEach(function() {
    parent = document.createElement('div');
    input = document.createElement('input');
    parent.appendChild(input);

    spyOn(TinyAutocomplete.prototype, 'onKeyUp').and.callThrough();
    spyOn(keyEvents.enterEvent, 'preventDefault');
    requestSpy = spyOn(TinyAutocomplete.prototype, 'request');
  });

  it('can change the query parameter', function() {
    var tinyAutocomplete = new TinyAutocomplete(input, {
      queryProperty: 'foo'
    });
    tinyAutocomplete.field.value = 'aa';
    tinyAutocomplete.onKeyUp();
    expect(requestSpy.calls.mostRecent().args[1]).toEqual({ foo: 'aa' });
  });

  it('can change the JSON processor', function() {
    var tinyAutocomplete = new TinyAutocomplete(input, {
      jsonProcessor: function(data) {
        return data.map(function(item, i) {
          return {
            title: item.title + i
          };
        });
      }
    });
    var testData = '[{"title": "foo"}, {"title": "bar"}]';
    var expectedString = '<input class="autocomplete-field" autocomplete="off"><ul class="autocomplete-list"><li class="autocomplete-item">foo0</li><li class="autocomplete-item">bar1</li></ul>';
    tinyAutocomplete.onSuccess({params: {q: 'aa'}}).bind(tinyAutocomplete)(200, testData);
    expect(tinyAutocomplete.container.innerHTML).toEqual(expectedString);
  });

  it('can change the minimum needed number of characters', function() {
    var tinyAutocomplete = new TinyAutocomplete(input, {
      minChars: 4
    });
    tinyAutocomplete.field.value = 'a';
    expect(tinyAutocomplete.onKeyUp()).toEqual(false);
    tinyAutocomplete.field.value = 'aa';
    expect(tinyAutocomplete.onKeyUp()).toEqual(false);
    tinyAutocomplete.field.value = 'aaa';
    expect(tinyAutocomplete.onKeyUp()).toEqual(false);
    tinyAutocomplete.field.value = 'aaaa';
    expect(tinyAutocomplete.onKeyUp()).toEqual(true);
  });

  it('can change the list template', function() {
    var tinyAutocomplete = new TinyAutocomplete(input, {
      listTemplate: '<div class="foo" />'
    });

    var testData = '[{"title": "foo"}, {"title": "bar"}]';
    var expectedString = '<input class="autocomplete-field" autocomplete="off"><div class="foo"><li class="autocomplete-item">foo</li><li class="autocomplete-item">bar</li></div>';
    tinyAutocomplete.onSuccess({params: {q: 'aa'}}).bind(tinyAutocomplete)(200, testData);
    expect(tinyAutocomplete.container.innerHTML).toEqual(expectedString);
  });

  it('can change the item templates', function() {
    var tinyAutocomplete = new TinyAutocomplete(input, {
      itemTemplate: '<i class="foo">{{title}}</i>'
    });

    var testData = '[{"title": "foo"}, {"title": "bar"}]';
    var expectedString = '<input class="autocomplete-field" autocomplete="off"><ul class="autocomplete-list"><i class="foo">foo</i><i class="foo">bar</i></ul>';
    tinyAutocomplete.onSuccess({params: {q: 'aa'}}).bind(tinyAutocomplete)(200, testData);
    expect(tinyAutocomplete.container.innerHTML).toEqual(expectedString);
  });

  it('always allows enter if specified', function() {
    var tinyAutocomplete = new TinyAutocomplete(input, {
      alwaysAllowEnter: true
    });

    tinyAutocomplete.field.dispatchEvent(keyEvents.enterEvent);
    expect(keyEvents.enterEvent.preventDefault).not.toHaveBeenCalled();
  });

  it('runs the user\'s custom onSelect callback when clicked', function() {
    var foo = undefined;
    var bar = undefined;
    var tinyAutocomplete = new TinyAutocomplete(input, {
      onSelectItem: function(target, val) { foo = target; bar = val; }
    });
    var testData = '[{"title": "foo"}, {"title": "bar"}]';
    var selectedItem;
    tinyAutocomplete.onSuccess({params: {q: 'aa'}}).bind(tinyAutocomplete)(200, testData);
    selectedItem = parent.querySelectorAll('.autocomplete-item')[1];

    // TODO: Since PhantomJS doesn't allow bubbling we'll have to find
    // some clever way of simulating a bubbling click. I'm out of ideas
    // how to do it so let's just trigger the click function directly
    tinyAutocomplete.onClick({
      preventDefault: function() {},
      target: selectedItem
    });

    expect(foo).toEqual(selectedItem);
    expect(bar).toEqual({ title: 'bar' });
  });

  it('marks all json hits as bold', function() {
    var tinyAutocomplete = new TinyAutocomplete(input, {
      markAsBold: true
    });

    var json = {
      foo: 'Lorem',
      bar: 'Ipsumlorem',
      baz: 'lorem lorem'
    };

    var strongJson = {
      foo: '<strong>Lor</strong>em',
      bar: 'Ipsum<strong>lor</strong>em',
      baz: '<strong>lor</strong>em <strong>lor</strong>em'
    };

    expect(tinyAutocomplete.markHitText(json, 'lor')).toEqual(strongJson);
  });

  it('doesn\'t re-mark the strong tags as bold', function() {
    var tinyAutocomplete = new TinyAutocomplete(input, {
      markAsBold: true
    });

    var json = {
      foo: 'ASTRONGSTRING',
      baz: 'a strong strong string'
    };

    var strongJson = {
      foo: 'A<strong>STR</strong>ONG<strong>STR</strong>ING',
      baz: 'a <strong>str</strong>ong <strong>str</strong>ong <strong>str</strong>ing'
    };

    expect(tinyAutocomplete.markHitText(json, 'str')).toEqual(strongJson);
  });

  it('can use local data instead of using a remote request', function() {
    var tinyAutocomplete = new TinyAutocomplete(input, {
      data: { title: 'foo' }
    });

    tinyAutocomplete.field.value = 'ba';
    tinyAutocomplete.onKeyUp();
    expect(tinyAutocomplete.request).not.toHaveBeenCalled();
  });

  it('sets the correct number of items when using local data', function() {
    var data = [
      { title: 'foo' },
      { title: 'bar' },
      { title: 'baz' }
    ];

    var tinyAutocomplete = new TinyAutocomplete(input, {
      data: data
    });

    tinyAutocomplete.field.value = 'ba';
    tinyAutocomplete.onKeyUp();
    expect(tinyAutocomplete.numItems).toEqual(2);
  });

  it('runs the user\'s onSelectItem callback when enter is pressed', function(done) {
    var data = [
      { title: 'foo' },
      { title: 'bar' },
      { title: 'baz' }
    ];

    var tinyAutocomplete = new TinyAutocomplete(input, {
      data: data,
      onSelectItem: function(selectedItem, b) {
        expect(b).toEqual({ title: 'bar' });
        done();
      }
    });

    tinyAutocomplete.field.value = 'ba';
    tinyAutocomplete.onKeyUp();
    expect(tinyAutocomplete.numItems).toEqual(2);
    tinyAutocomplete.field.dispatchEvent(keyEvents.arrowDownEvent);
    tinyAutocomplete.field.dispatchEvent(keyEvents.enterEvent);
  });

  it('runs the user\'s onSelectItem callback when an item is clicked', function(done) {
    var data = [
      { title: 'foo' },
      { title: 'bar' },
      { title: 'baz' }
    ];

    var tinyAutocomplete = new TinyAutocomplete(input, {
      data: data,
      onSelectItem: function(selectedItem, b) {
        expect(b).toEqual({ title: 'bar' });
        done();
      }
    });

    tinyAutocomplete.field.value = 'ba';
    tinyAutocomplete.onKeyUp();
    expect(tinyAutocomplete.numItems).toEqual(2);
    tinyAutocomplete.field.dispatchEvent(keyEvents.arrowDownEvent);
    tinyAutocomplete.field.dispatchEvent(keyEvents.enterEvent);
  });
});
