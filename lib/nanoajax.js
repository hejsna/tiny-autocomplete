// https://github.com/yanatan16/nanoajax
// With a bunch of modifications to use as standard library for
// ajax in TinyAutocomplete

/* global TinyAutocomplete */
(function() {
  var nanoajax = function(params) {
    if (typeof params == 'string') params = {url: params}
    var headers = params.headers || {};
    var body = params.data;
    var method = params.type || (body ? 'POST' : 'GET');
    var callback = params.success;
    var withCredentials = params.withCredentials || false;

    var req = getRequest()

    req.onreadystatechange = function() {
      if (req.readyState == 4)
        callback(req.status, req.responseText, req)
    }

    if (body) {
      setDefault(headers, 'Content-Type', 'application/x-www-form-urlencoded')
    }

    req.open(method, params.url, true)

    // has no effect in IE
    // has no effect for same-origin requests
    // has no effect in CORS if user has disabled 3rd party cookies
    req.withCredentials = withCredentials

    for (var field in headers)
      req.setRequestHeader(field, headers[field])

    req.send(body)
  }

  function getRequest() {
    if (window.XMLHttpRequest)
      return new window.XMLHttpRequest;
    else {
      try {
        return new window.ActiveXObject('MSXML2.XMLHTTP.3.0');
      } catch (e) {}
    }

    throw new Error('no xmlhttp request able to be created')
  }

  function setDefault(obj, key, value) {
    obj[key] = obj[key] || value
  }

  TinyAutocomplete.prototype.$ = {
    ajax: nanoajax
  };
})();
